//
//  ofxOpenPoseUIKit.hpp
//  ofxOpenPoseToolkit
//
//  Created by Qinzi Tan on 5/30/19.
//

#ifndef DataConstruct_hpp
#define DataConstruct_hpp

#include "ofMain.h"
#include "ofxJSON.h"

#include "ofxFrame.hpp"
#include "ofxBody.hpp"
#include "ofxBodyPart.hpp"

class ofxOpenPoseUIKit{
    
public:
    bool fileOpenSuccess;
    bool dataReady;
    
    void analyze(string JSON_FOLDER_PATH,string JSON_FILE_PREFIX, int FRAME_COUNTS);
    
    ofxFrame parseData(ofxJSONElement json);
    
    vector<ofxFrame> frames;
    
    string bodyPartNames[26] = {
        "NOSE",
        "NECK",
        "SHOULDER_R",
        "ELBOW_R",
        "WRIST_R",
        "SHOULDER_L",
        "LElBOW",
        "WRIST_L",
        "MIDHIP",
        "HIP_R",
        "KNEE_R",
        "ANKLE_R",
        "HIP_L",
        "KNEE_L",
        "ANKLE_L",
        "EYE_R",
        "EYE_L",
        "EAR_R",
        "EAR_L",
        "BIGTOE_L",
        "SMALLTOE_L",
        "HEEL_L",
        "BIGTOE_R",
        "SMALLTOE_R",
        "HEEL_R",
        "BACKGROUND"
    };
    
};

#endif /* ofxOpenPoseUIKit_hpp */
