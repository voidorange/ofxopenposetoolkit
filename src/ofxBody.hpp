//
//  ofxBody.hpp
//  ofxOpenPoseToolkit
//
//  Created by Qinzi Tan on 5/30/19.
//

#ifndef ofxBody_hpp
#define ofxBody_hpp

#include "ofMain.h"
#include "ofxBodyPart.hpp"

class ofxBody{
public:
    vector<ofxBodyPart> bodyParts;
};

#endif /* ofxBody_hpp */
