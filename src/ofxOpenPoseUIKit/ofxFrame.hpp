//
//  ofxFrame.hpp
//  ofxOpenPoseToolkit
//
//  Created by Qinzi Tan on 5/30/19.
//

#ifndef ofxFrame_hpp
#define ofxFrame_hpp

#include "ofMain.h"
#include "ofxBody.hpp"

class ofxFrame{
public:
    vector<ofxBody> bodies;
};

#endif /* ofxFrame_hpp */
