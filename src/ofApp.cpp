#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetFrameRate(60);
    ofSetWindowShape(1300, 908);
    ofBackground(5);
    ofSetCircleResolution(120);
    ofEnableSmoothing();
    ofSetBackgroundAuto(true);
    
    setupUI();
    
}

//--------------------------------------------------------------
void ofApp::update(){
    video.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
    //    cout<<opKit.dataReady<<endl;
    if(opKit.dataReady && !video.isPlaying()) {
        video.play();
        cout<<"here?"<<endl;
    }
    
    video.draw(0,0, video.getWidth(), video.getHeight());
    
}

//--------------------------------------------------------------
void ofApp::load(){
    video.load(VIDEO_FILE_PATH);
    video.setLoopState(OF_LOOP_NORMAL);
    
    opKit.analyze(JSON_FOLDER_PATH,JSON_FILE_PREFIX,ofToInt(FRAME_COUNTS));
}

//--------------------------------------------------------------
void ofApp::onTextInputEvent(ofxDatGuiTextInputEvent e)
{
    // text input events carry the text of the input field //
    cout << "From Event Object: " << e.text << endl;
    // although you can also retrieve it from the event target //
    cout << "From Event Target: " << e.target->getText() << endl;
    
    if(e.target->getLabel() == "VIDEO_FILE_PATH"){
        VIDEO_FILE_PATH = e.target->getText();
    }
    else if(e.target->getLabel() == "JSON_FOLDER_PATH"){
        JSON_FOLDER_PATH = e.target->getText();
    }
    else if(e.target->getLabel() == "JSON_FILE_PREFIX"){
        JSON_FILE_PREFIX = e.target->getText();
    }
    else if(e.target->getLabel() == "FRAME_COUNTS"){
        FRAME_COUNTS = e.target->getText();
    }
    
}

//--------------------------------------------------------------
void ofApp::onButtonEvent(ofxDatGuiButtonEvent e)
{
    if(e.target->getLabel() == "IMPORT"){
        load();
    }
}

//--------------------------------------------------------------
void ofApp::onMatrixEvent(ofxDatGuiMatrixEvent e)
{
    cout << "onMatrixEvent " << e.child << " : " << e.enabled << endl;
    cout << "onMatrixEvent " << e.target->getLabel() << " : " << e.target->getSelected().size() << endl;
}

//--------------------------------------------------------------
void ofApp::setupUI(){

    gui = new ofxDatGui(ofxDatGuiAnchor::TOP_RIGHT);
    gui->addTextInput("VIDEO_FILE_PATH", "PATH TO VIDEO FILE");
    gui->addTextInput("JSON_FOLDER_PATH", "PATH TO JSON FOLDER");
    gui->addTextInput("JSON_FILE_PREFIX", "PREFIX IN JSON FILE NAME (OPTIONAL)");
    gui->addTextInput("FRAME_COUNTS", "TOTAL NUMBER OF JSON FILES");
    gui->addButton("IMPORT");
    
    gui->addMatrix("Joints", 26, true);
    
    gui->onButtonEvent(this, &ofApp::onButtonEvent);
    gui->onTextInputEvent(this, &ofApp::onTextInputEvent);
    gui->onMatrixEvent(this, &ofApp::onMatrixEvent);
    
    themes = {  new ofxDatGuiTheme(true),
        new ofxDatGuiThemeSmoke(),
        new ofxDatGuiThemeWireframe(),
        new ofxDatGuiThemeMidnight(),
        new ofxDatGuiThemeAqua(),
        new ofxDatGuiThemeCharcoal(),
        new ofxDatGuiThemeAutumn(),
        new ofxDatGuiThemeCandy()};
    int tIndex = 0;
    gui->setTheme(themes[tIndex]);
    
}
