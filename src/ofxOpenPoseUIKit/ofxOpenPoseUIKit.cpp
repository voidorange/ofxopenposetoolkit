//
//  ofxOpenPoseUIKit.cpp
//  ofxOpenPoseToolkit
//
//  Created by Qinzi Tan on 5/30/19.
//

#include "ofxOpenPoseUIKit.hpp"
void ofxOpenPoseUIKit::analyze(string JSON_FOLDER_PATH,string JSON_FILE_PREFIX, int FRAME_COUNTS){
    
    cout<<"start analyze"<<endl;
    
    frames.clear();
    
    for(int i = 0; i<FRAME_COUNTS; i++){
        ofxFrame f;
        ofxJSONElement jsonElement;
        
        string file = JSON_FOLDER_PATH + JSON_FILE_PREFIX + std::to_string(i) + ".json";
        
        bool success = jsonElement.open(file);
        if (success) {
            cout << "json " + std::to_string(i) + " parsed successfully."  << endl;
            fileOpenSuccess = true;
            
        } else {
            cout << "Failed to parse JSON" << endl;
            fileOpenSuccess = false;
        }
        
        f = parseData(jsonElement);
        
        frames.push_back(f);
    }
    
    if (fileOpenSuccess) {
        dataReady = true;
        cout<< "FINISHED PARSING ALL FILES" << endl;
        
    }
    
}

//--------------------------------------------------------------
ofxFrame ofxOpenPoseUIKit::parseData(ofxJSONElement json){
    
    ofxFrame f;
    vector<ofxBody> bodies;
    
    for(int i = 0; i < json["people"].size(); i++){
        ofxBody body;
        int bpIndex = 0;
        
        if(bpIndex > 25) return;
        
        for(int k = 0; k< json["people"][i]["pose_keypoints_2d"].size(); k+=3){
            float _x = json["people"][i]["pose_keypoints_2d"][k].asFloat();
            float _y = json["people"][i]["pose_keypoints_2d"][k+1].asFloat();
            float _c = json["people"][i]["pose_keypoints_2d"][k+2].asFloat();
            string _name = bodyPartNames[bpIndex];
            ofPoint _pose2D(_x,_y);
            
            ofxBodyPart bp;
            bp.pose2D = _pose2D;
            bp.c = _c;
            bp.name = _name;
            
            body.bodyParts.push_back(bp);
            
            bpIndex ++;
        }
        
        bodies.push_back(body);
    }
    
    f.bodies = bodies;
    
    return f;
    
}
