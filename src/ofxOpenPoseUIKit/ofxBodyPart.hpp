//
//  ofxBodyPart.hpp
//  ofxOpenPoseToolkit
//
//  Created by Qinzi Tan on 5/30/19.
//

#ifndef ofxBodyPart_hpp
#define ofxBodyPart_hpp

#include "ofMain.h"

class ofxBodyPart{
public:
    string name;
    ofPoint pose2D;
    float c; //confidence
};

#endif /* ofxBodyPart_hpp */
