#pragma once

#include "ofMain.h"
#include "ofxJSON.h"
#include "ofxDatGui.h"

#include "ofxOpenPoseUIKit.hpp"


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
    
        void setupUI();
    
        void load();
    
    ofVideoPlayer video;
    ofxOpenPoseUIKit opKit;
    
    void onTextInputEvent(ofxDatGuiTextInputEvent e);
    void onButtonEvent(ofxDatGuiButtonEvent e);
    void onMatrixEvent(ofxDatGuiMatrixEvent e);
    
    
    string VIDEO_FILE_PATH;
    string JSON_FOLDER_PATH;
    string JSON_FILE_PREFIX;
    string FRAME_COUNTS;

    vector<ofxDatGuiTheme*> themes;
    
    ofxDatGui* gui;
    
    
};
